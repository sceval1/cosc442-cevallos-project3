package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineItemTest {
	
	/**
	 * Declaring  necessary test objects for VM class 
	 * 
	 */
	VendingMachineItem itemA;
	VendingMachineItem itemD;
	
	/**
	 * Initializing the Item that has been created to conduct the test
	 * @throws Exception
	 */

	@Before
	public void setUp() throws Exception {
		itemA = new VendingMachineItem("ItemA", 1.00);
	}
	
	@Test
	/**
	 * Test for the constructor to ensure that it can add a item  {@link VendingMachineItem} class.
	 */
	public void testVendingMachineItem() {
		assertEquals("ItemA", itemA.getName());
		assertEquals(1.00, itemA.getPrice(), 0.0001);	
	}
	
	@Test(expected = VendingMachineException.class)
	/**
	 * Test that that the constructor does not permit a item to be less than $1.00
	 */
	public void testLessVendingMachineItem() {
		itemD = new VendingMachineItem("ItemD", -1.00);
	}
	
	@Test
	/**
	 * Test the getting of a name from an item within the class
	 */
	public void testGetName() {
		assertEquals("ItemA", itemA.getName());
		
	}
	
	@Test
	/**
	 * Test the getting of a price from an item within the class  
	 */
	public void testGetPrice() {
		assertEquals(1.00, itemA.getPrice(), 0.000);
	}
	
	/**
	 * Cleans up test objects after the tests have been executed
	 * @throws Exception
	 */
	
	@After
	public void tearDown() throws Exception {
		itemA = null;
		itemD = null; 
	}
}
