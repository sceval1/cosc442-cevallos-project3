package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineTest {

	/**
	 * Declaring  necessary test objects for VM class 
	 * 
	 */
	
	VendingMachine vm;
	VendingMachineItem itemB;
	VendingMachineItem itemC;

	
	/**
	 * Preparing the VM to conduct proper test on the object 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		vm = new VendingMachine();
		itemB = new VendingMachineItem("ItemB", 1.00);
		itemC = new VendingMachineItem("ItemC", 2.00);
	}
	
	
	/**
	 * Testing methods within the class that allow the vending machine to function  {@link VendingMachine} class
	 */
	
	@Test
	/**
	 * Testing AddItem method and seeing if it can add a a valid item 
	 */
	public void testAddItem() {
		vm.addItem(itemB, "A");
		assertEquals(itemB,vm.getItem("A"));
	}
	
	@Test
	/**
	 * Testing the other return statements that get assigned
	 */
	public void testOtherCodes() {
		vm.addItem(itemB, "C");
		vm.addItem(itemC, "D");
	}
	
	@Test(expected = VendingMachineException.class)
	/**
	 * Testing if AddItem allows to add an item to a already used code
	 */
	
	public void testFilledAddItem() {
		vm.addItem(itemB, "A");
		vm.addItem(itemC, "A"); 
		
	}
	
	@Test(expected = VendingMachineException.class)
	/**
	 * Testing if AddItem allows to add a invalid code
	 */
	public void testInvalidAddItem() {
		vm.addItem(itemB, "S");
	}

	@Test
	/**
	 * Testing to see if RemoveItem allows to remove a item that is real
	 */
	public void testRemoveItem() {
		vm.addItem(itemB, "A");
		assertEquals(itemB, vm.removeItem("A"));
	}
	
	@Test(expected = VendingMachineException.class)
	/**
	 * Testing to see if RemoveItem allows to remove a item that does not exist
	 */
	public void testEmptyRemoveItem() {
		assertEquals(itemB,vm.removeItem("A"));
	}
	
	@Test
	/**
	 * Testing to add a positive amount of money with InsertMoney
	 */
	public void testInsertMoney() {
		vm.insertMoney(5.00);
		assertEquals(5.00,vm.getBalance(),0.001);
	}
	
	@Test(expected = VendingMachineException.class)
	/**
	 * Testing to add a negative amount of money with InsertMoney
	 */
	public void testNegInsertMoney() {
		vm.insertMoney(-1.00);
	}
	
	@Test
	/**
	 * Testing to get the Balance after adding money into the vending machine
	 */
	public void testGetBalance() {
		vm.insertMoney(1.00);
		assertEquals(1.00,vm.getBalance(),0.001);
	}
	
	@Test
	/**
	 * Testing to get a balance of 0.00 when the vending machine has not had money added
	 */
	public void testZeroGetBalance() {
		assertEquals(0.00,vm.getBalance(),0.001);
	}
	
	@Test
	/**
	 * Testing to make a valid purchase with money and a valid item
	 */
	public void testMakePurchase() {
		vm.insertMoney(5.00);
		vm.addItem(itemB, "A");
		assertEquals(true,vm.makePurchase("A"));
	}
	
	@Test
	/**
	 * Testing to make a purchase without enough money
	 */
	public void testBrokeMakePurchase() {
		vm.insertMoney(1.00);
		vm.addItem(itemC, "A");
		assertEquals(false,vm.makePurchase("A"));
	}
	
	@Test
	/**
	 * Testing to make a purchase for a fake item
	 */
	public void testFakeMakePurchase() {
		vm.insertMoney(1.00);
		vm.addItem(itemB, "A");
		assertEquals(false,vm.makePurchase("B"));
	}

	@Test
	/**
	 * Testing to return the total change amount needed
	 */
	public void testReturnChange() {
		vm.insertMoney(2.00);
		vm.addItem(itemB, "A");
		assertEquals(2.00,vm.getBalance(),0.001);
		vm.makePurchase("A");
		assertEquals(1.00,vm.returnChange(),0.001);
	}
	

	
	@After
	/**
	 * Removes the created Vending Machine and Items 
	 * @throws Exception
	 */
	public void tearDown() throws Exception {
		vm = null;
		itemB = null;
		itemC = null;
	}


}
